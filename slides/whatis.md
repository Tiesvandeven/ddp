# Object Oriented 
vs
# Functional Programming
vs
# Data Oriented Programming

---slide---

## What is OO

* Everything is an object
* Encapsulation
* Boundaries
* Inheritance

---slide---

## What is OO

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%" data-line-numbers="1-10|1,6,8,10">class Person(
    private val name : String,
    private val age : Int
) {

  fun getAge() = age
	
  fun getName() = name
  
  fun doBusinessLogic() = TODO()

}
</code>
</pre>

---slide---

## What is FP

* Everything is a function
* Higher order functions
* Building blocks
* Composition

---slide---

## What is DOP

* Similar to FP
* Everything is data

---slide---

## What is DOP

<pre>
<code class="hljs kotlin" style="max-height: 100%;font-size:140%" data-line-numbers="1-6|2,3">data class Person(
  val name : String,
  val age : Int
)
</code>
</pre>
